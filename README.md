List of 1392 English words with their simple present and simple past forms. The data was downloaded from
http://www.cs.utexas.edu/users/ml/nldata/pastTenseData.html

This repository contains a python script to convert the Prolog form to CSV. The CSV can then be converted to other data formats using online tools like [this](https://www.csvjson.com/csv2json)

### Usage

Pipe the stdout to a file to save CSV as file

```
$ python verbPastLookup.py >> wordlist.csv
```
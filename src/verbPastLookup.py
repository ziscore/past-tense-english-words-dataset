# convert prolog form to normie csv
# "past(['A', 'B', 'A', 'N', 'D', 'O', 'N'],['A', 'B', 'A', 'N', 'D', 'O', 'N', 'E', 'D']).\n"
# becomes
# abandon, abandoned

word = ''
addToWord = 0

# please check this path before running the script
with open('../data/alphabetic-past-data', 'r') as fp:
	for cnt, line in enumerate(fp):
		for x in line:
			if (x == '['):
				addToWord = 1
				continue
			if (x == ']'):
				print(word, end=',')
				addToWord = 0
				word=''
				continue

			if addToWord:
				if (x in ("'", " ", ",")):
					continue
				x = x.lower()
				word += x

			if(x == "\n"):
				print()